+++
date = "2018-06-25"
weight = 100

title = "sdk-performance-tools-sysprof-smoke-test"

aliases = [
    "/qa/test_cases/sdk-performance-tools-sysprof.md",
    "/old-wiki/QA/Test_Cases/sdk-performance-tools-sysprof",
    "/old-wiki/QA/Test_Cases/sdk-performance-tools-sysprof-smoke-test"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
