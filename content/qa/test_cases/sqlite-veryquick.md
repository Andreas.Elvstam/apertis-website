+++
date = "2018-07-03"
weight = 100

title = "sqlite-veryquick"

aliases = [
    "/old-wiki/QA/Test_Cases/sqlite-veryquick"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
