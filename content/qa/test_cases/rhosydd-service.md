+++
date = "2017-01-10"
weight = 100

title = "rhosydd-service"

aliases = [
    "/qa/test_cases/rhosydd.md",
    "/old-wiki/QA/Test_Cases/rhosydd",
    "/old-wiki/QA/Test_Cases/rhosydd-service"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
