+++
date = "2015-11-05"
weight = 100

title = "grilo-playlist-browsing"

aliases = [
    "/old-wiki/QA/Test_Cases/grilo-playlist-browsing"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
