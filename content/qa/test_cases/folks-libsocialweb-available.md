+++
date = "2017-10-27"
weight = 100

title = "folks-libsocialweb-available"

aliases = [
    "/old-wiki/QA/Test_Cases/folks-libsocialweb-available"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
