+++
date = "2016-06-01"
weight = 100

title = "updater"

aliases = [
    "/old-wiki/QA/Test_Cases/updater"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
